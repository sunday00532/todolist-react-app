import React from 'react';
import { useState } from "react";

function AddList( { addTask } ) {

    const [event, setList] = useState("");
    function listAddChange(e) {
        setList(e.target.value);
    }

    async function addList() {      
        console.log(event)
        try {
          await addTask(event)
          alert('新增成功')
        } catch (error) {
          const errorData = error.response.data
          if (errorData && errorData.error) {
            alert(errorData.error[0]);
          } else {
            alert(errorData.message);
          }
        }
      }


    return (
        <form onSubmit={addList} className='inputBox'>
            <input type="text" placeholder="請輸入待辦事項" value={event} onChange={listAddChange}></input>

            <button 
                type="submit" 
                className="absolute w-10 h-10 rounded bg-grey-3 top-1 right-1"
            >
            新增
        </button>
        </form>
    )
}

export default AddList;