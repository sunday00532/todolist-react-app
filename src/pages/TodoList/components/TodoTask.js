import React from 'react';
import CheckedIcon from '../../../assets/Checked.png';
import CloseIcon from '../../../assets/Close.png';

function TodoTask ( {task , setTaskDone, removeTask } ) {
    return (
        <li>
            <label className='todoList_label'>
                {
                    task.completed_at 
                    ? <img src={CheckedIcon} className="checkDone" onClick={setTaskDone}></img>
                    : <input type="checkbox" className="todoList_input" onClick={setTaskDone}>
                    </input>
                }
                <span>
                    {task.content}
                </span>
                <a>
                    <i className='fa fa-times'></i>
                </a>
            </label>
            <img src={CloseIcon} className="removeTask" onClick={removeTask}>
                 
            </img>
        </li>
    )
}

export default TodoTask;