import React from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { useState } from 'react';
import { API_LOGIN } from '../../../global/constants'
import { useApi } from '../../../global/useApi'

const SignIn = ({ setUser }) => {

  const navigate = useNavigate();

    const [email, setEmail] = useState("");
    function emailChange(e) {
      setEmail(e.target.value);
    }

    const [password, setPwd] = useState("");
    function pwdChange(e) {
      setPwd(e.target.value);
    }

    const data = {user: {
        email,
        password
      }
    }

    async function login() {      
      try {
        const rsp = (await useApi().post(API_LOGIN, data)).data
        setUser(rsp.email)
        navigate('/');
      } catch (error) {
        const errorData = error.response.data
        if (errorData && errorData.error) {
          alert(errorData.error[0]);
        } else {
          alert(errorData.message);
        }
      }
    }

    return (
      <div id="loginPage" className="bg-yellow">
      <div className="conatiner loginPage vhContainer ">
        <div className="side">
              <a href="#"><img className="logoImg" src="https://upload.cc/i1/2022/03/23/rhefZ3.png" alt=""></img></a>
              <img className="d-m-n" src="https://upload.cc/i1/2022/03/23/tj3Bdk.png" alt="workImg"></img>
        </div>
        <div>
            <form className="formControls" action="index.html">
                  <h2 className="formControls_txt">最實用的線上代辦事項服務</h2>
                  <label className="formControls_label">Email</label>
                  <input className="formControls_input" 
                    type="text" 
                    id="email" 
                    name="email" 
                    placeholder="請輸入 email" 
                    value={email} 
                    onChange={emailChange} required>
                  </input>
                  {/* <span>此欄位不可留空</span> */}
                  <label className="formControls_label">密碼</label>
                  <input className="formControls_input" 
                    type="password" 
                    name="pwd" 
                    id="pwd" 
                    placeholder="請輸入密碼" 
                    value={password} 
                    onChange={pwdChange} required>
                  </input>
                  <input className="formControls_btnSubmit" type="button" onClick={login} value="登入"></input>
                  <Link to="/signUp">
                    <span className='formControls_btnLink'>
                      註冊帳號
                    </span>
                  </Link>
            </form>
        </div>
      </div>
    </div>
      );
}

export default SignIn;