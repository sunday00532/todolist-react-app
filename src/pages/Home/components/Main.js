import React from 'react';
import { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import useTodoList  from '../../../global/useToddoList'
import AddList from '../../TodoList/components/AddList';
import TodoTask from '../../TodoList/components/TodoTask';
import { apiGetTodos } from '../../../global/todoApi';
import { useApi } from '../../../global/useApi'
import { API_LOGOUT } from '../../../global/constants'
import { tabOptions } from '../../../global/tabOptions'


function Main( {userName} ) {
    
    const {
        todoList,
        setTodoList,
        refreshId,
        isDone,
        removeTask,
        clearDone,
        addTask,
      } = useTodoList();

      const navigate = useNavigate();
      const [currentTab, setCurrentTab] = useState(tabOptions[0].value);
      var filterTodoList = []

      switch(currentTab)
      {   
        case tabOptions[0].value :
            filterTodoList = todoList.filter(item => item)
            break;
        case tabOptions[1].value :
            filterTodoList = todoList.filter(item => !item.completed_at)
            break;
        case tabOptions[2].value :
            filterTodoList = todoList.filter(item => item.completed_at)
            break;
       }

       console.log('currentTab:',currentTab);

       console.log('filterTodoList:',filterTodoList);

      const captionText = currentTab == tabOptions[1].value
      ? `${todoList.filter(item => !item.completed_at).length} 個未完成項目`
      : `${todoList.filter(item => item.completed_at).length} 個已完成項目`; 
    
      async function fetchTodos() {
        try {
          const source = (await apiGetTodos()).data;
          console.log('todoList',source);
          setTodoList(source.todos);
        } catch (error) {
          console.log(error.message)
        }
      }

      async function logout() {
          try {
            await useApi().delete(API_LOGOUT)
            localStorage.removeItem('token');
            navigate('/signin');
          } catch (error) {
          alert(error.message)
          }
      }

      //執行useState之後  會強制整個component 重新渲染
      useEffect(() => {
        fetchTodos();
      }, [refreshId])


    return(
        <div id="todoListPage" className="bg-half">
        <nav>
            <h1><a href="#"><img className="logoImg" src="https://upload.cc/i1/2022/03/23/rhefZ3.png" alt=""></img></a></h1>
            <ul>
                <li className="todo_sm"><a href="#"><span>{`${userName}的代辦`}</span></a></li>
                <li><a href="#loginPage" onClick={logout}>登出</a></li>
            </ul>
        </nav>
        <div className="conatiner todoListPage vhContainer">
            <div className="todoList_Content">
                <AddList addTask = { addTask }/>
                <div className="todoList_list">
                    <ul className="todoList_tab">
                        {
                            tabOptions.map(tab => (
                                <a 
                                key={tab.key} 
                                className={tab.value === currentTab ? 'active' : ''} 
                                onClick={() => setCurrentTab(tab.value)}>
                                    {tab.name}
                                </a>
                            ))
                        }
                    </ul>

                    <div className="todoList_items">
                        <ul className="todoList_item">
                            {
                                filterTodoList.length > 0 
                                ? filterTodoList.map((item) => (
                                    <TodoTask
                                        key={item.id}
                                        setTaskDone = {() => isDone(item.id)}
                                        removeTask = {() => removeTask(item.id)}
                                        task={item}
                                    />
                                ))
                                :
                                <div className='noneData'>None</div>
                            }
                        </ul>
                        <div className="todoList_statistics">
                            <p> { captionText }</p>
                            <span onClick={clearDone}>清除已完成項目</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    );
}

export default Main;