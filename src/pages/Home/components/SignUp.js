import React from 'react';
import { useState } from "react";
import { Link, useNavigate } from 'react-router-dom';
import { API_REGISTER } from '../../../global/constants'
import { useApi } from '../../../global/useApi'


const SignUp = () => {
    const navigate = useNavigate();

    const [email, setEmail] = useState("");
    function emailChange(e) {
      setEmail(e.target.value);
    }

    const [password, setPwd] = useState("");
    function pwdChange(e) {
      setPwd(e.target.value);
    }

    const [nickName, setNickName] = useState("");
    function nickNameChange(e) {
      setNickName(e.target.value);
    }

    const data = {user: {
        email,
        nickName,
        password
      }
    }

    async function register() {      
      try {
        await useApi().post(API_REGISTER, data)
        navigate('/');
      } catch (error) {
        const errorData = error.response.data
        if (errorData && errorData.error) {
          alert(errorData.error[0]);
        } else {
          alert('輸入內容錯誤');
        }
      }
    }


    return (
      <div id="signUpPage" className="bg-yellow">
      <div className="conatiner signUpPage vhContainer">
          <div className="side">
              <a href="#"><img className="logoImg" src="https://upload.cc/i1/2022/03/23/rhefZ3.png" alt=""></img></a>
              <img className="d-m-n" src="https://upload.cc/i1/2022/03/23/tj3Bdk.png" alt="workImg"></img>
          </div>
          <div>
              <form className="formControls" action="index.html">
                  <h2 className="formControls_txt">註冊帳號</h2>
                  <label className="formControls_label" for="email">Email</label>
                  <input className="formControls_input" type="text" id="email" name="email" placeholder="請輸入 email" value={email} onChange={emailChange} required></input>
                  <label className="formControls_label" for="name">您的暱稱</label>
                  <input className="formControls_input" type="text" name="name" id="name" placeholder="請輸入您的暱稱" value={nickName} onChange={nickNameChange}></input>
                  <label className="formControls_label" for="pwd">密碼</label>
                  <input className="formControls_input" type="password" name="pwd" id="pwd" placeholder="請輸入密碼" value={password} onChange={pwdChange} required></input>
                  <label className="formControls_label" for="pwd">再次輸入密碼</label>
                  <input className="formControls_input" type="password" name="pwd" id="pwd" placeholder="請再次輸入密碼" required></input>
                  <input className="formControls_btnSubmit" type="button" onClick={register} value="註冊帳號"></input>
                  <Link to="/signIn">
                    <span className='formControls_btnLink'>
                      登入
                    </span>
                  </Link>
              </form>
          </div>
      </div>
    </div>
  );
}

export default SignUp;