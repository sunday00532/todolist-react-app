import React from 'react';
import { Routes, Route } from 'react-router-dom';
import SignIn from "./components/SignIn";
import SignUp from "./components/SignUp";
import Main from './components/Main';
import RouteGuard from '../../components/RouteGuard';
import { useState } from 'react';

const App = () => {

  const [user, setUser] = useState('');

    return (
      <Routes>
        <Route element={<RouteGuard />}>
          <Route path="/" element={<Main userName= { user }/>} />
          <Route path="/signIn" element={<SignIn setUser={setUser}/>} />
          <Route path="/signUp" element={<SignUp />} />
        </Route>
    </Routes>
  );
}

export default App;