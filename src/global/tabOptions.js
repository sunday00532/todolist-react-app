export const tabOptions = Object.freeze([
  {
    name: '全部',
    value: 0,
    key: 'All'
  },
  {
    name: '待完成',
    value: 1,
    key: 'Todo'
  },
  {
    name: '已完成',
    value: 2,
    key: 'Done'
  }]);