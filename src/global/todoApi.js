import { useApi } from './useApi';

export function apiGetTodos() {
    return useApi().get('/todos');
  }
  
  export function apiPostTodos(data) {
    return useApi().post('/todos', data);
  }
  
  export function apiEditTodos(id, data) {
    return useApi().put(`/todos/${id}`, data);
  }
  
  export function apiDeleteTodos(id) {
    return useApi().delete(`/todos/${id}`);
  }
  
  export function apiToggleTodoIsDone(id) {
    return useApi().patch(`/todos/${id}/toggle`);
  }