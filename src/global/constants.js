export const API_HOST = "https://todoo.5xcamp.us"
export const API_LOGIN = `/users/sign_in`
export const API_CHECK = `/check`
export const API_REGISTER = `/users`
export const API_LOGOUT = `/users/sign_out`

export const API_ADDLIST = `/todos`