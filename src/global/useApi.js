import axios from "axios";
import { API_HOST } from  './constants'

export function useApi() {
    const instance = axios.create({
      baseURL: API_HOST
    });
  
    instance.interceptors.request.use((config) => {
        const token = localStorage.getItem('token');
        if (token) {
            console.log('token',token)
            config.headers.authorization = `Bearer ${token}`;
        }
        return config;
      });
    
      instance.interceptors.response.use((config) => {
        console.log(config);
        const token = config.headers.authorization;
        if (token) {
            localStorage.setItem('token', token.split('Bearer')[1].trim());
        }
        return config;
      });

    return instance;
  }
