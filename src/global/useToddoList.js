import { useState } from 'react';
import { apiPostTodos , apiToggleTodoIsDone, apiDeleteTodos } from './todoApi';

export default function useTodoList() {

  const [todoList, setTodoList] = useState([]);
  const [refreshId, setRefreshId] = useState('');

  async function addTask(content) {
    try {
        await apiPostTodos({ todo: {content} })
        setRefreshId(Date.now() + Math.random().toString(16).slice(2))
    } catch (error) {
        console.log(error.message);
    }
  }

  async function isDone(id) {
    try {
        await apiToggleTodoIsDone(id)
        setRefreshId(Date.now() + Math.random().toString(16).slice(2))
    } catch (error) {
        console.log(error.message);
    }
  } 

  async function removeTask(id) {
    try {
        await apiDeleteTodos(id)
        setRefreshId(Date.now() + Math.random().toString(16).slice(2))
    } catch (error) {
        console.log(error.message);
    }
  } 

  async function clearDone() {
    try {
      const promiseStack = todoList
        .filter(item => item.completed_at)
        .reduce((prev, curr) => {
          prev.push(apiDeleteTodos(curr.id))
          return prev;
        }, []);
  
      await Promise.all(promiseStack);
      setRefreshId(Date.now() + Math.random().toString(16).slice(2))
    } catch (error) {
      console.log(error.message)
    }
  }

    return {
        todoList,
        setTodoList,
        refreshId,
        isDone,
        removeTask,
        clearDone,
        addTask
    }
}