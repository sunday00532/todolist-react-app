import React from 'react';
import { Outlet, useLocation, useNavigate } from 'react-router-dom';
import { useEffect, useState } from 'react';
import { useApi } from '../global/useApi'
import { API_CHECK } from '../global/constants'

export default function RouteGuard() {
    const location = useLocation();
    const navigate = useNavigate();
    const [token, setToken] = useState('');
  
    useEffect(() => {
      checkAuth();
    }, [token]);
  
    async function checkAuth() {
        if (token && location.pathname.match(/^\/sign(in|up)/i)) {
            return navigate('/');
        }
        try {
            await useApi().get(API_CHECK);
            const localToken = localStorage.getItem('token');
            setToken(localToken);
        } catch(error) {
            navigate('/signIn');
        }
    }
  
    return <Outlet />
  }